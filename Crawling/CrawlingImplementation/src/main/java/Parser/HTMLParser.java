package Parser;


import Anunt.Location;
import Anunt.Anunt;
import Anunt.InterestPoint;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by donic on 4/8/2016.
 */
public class HTMLParser {

    private Document pageToParse;
    private Anunt anunt = new Anunt();

    public HTMLParser(Document doc){
        pageToParse = doc;
    }
    public void parsePage(){

        this.parseCoordinates();
        this.parseInfos();
        this.parseInterestPoints();
    }
    public void parseCoordinates(){
        InterestPoint tempIP;
        Location tempLoc = new Location(0,0);


        Elements elements  = pageToParse.select("script");

        Pattern coordPatt = Pattern.compile("google.maps.LatLng\\(([^ ].+?[^ ]),([^ ].+?[^ ])\\);");
        Pattern interestPatt = Pattern.compile("title: '(.+?)'");

        Matcher interestMatch = interestPatt.matcher(elements.html());
        Matcher coordMatch = coordPatt.matcher(elements.html());
            if(coordMatch.find() == true){
                tempLoc = new Location(Double.parseDouble(coordMatch.group(1)),Double.parseDouble(coordMatch.group(2)));
                anunt.setLocation(tempLoc);
            }
        while(coordMatch.find() && interestMatch.find()){
              tempLoc = new Location(Double.parseDouble(coordMatch.group(1)),Double.parseDouble(coordMatch.group(2)));
              tempIP = new InterestPoint(interestMatch.group(1),tempLoc);
              anunt.addInteresPoint(tempIP);
        }


    }
    public void parseInfos(){
        int price;

        Elements elements = pageToParse.select("div[class=col-xs-12 col-sm-6]");
        Elements title = pageToParse.select("div[class=col-lg-12]");
        Elements infoSupl = pageToParse.select("section");

        Element caractDiv = elements.first();

        Pattern infoPatt = Pattern.compile("even red\"><strong>.+?</strong>\\s(\\w+)\\s+(\\d+)\\D+</li>");
        Matcher infoMatch = infoPatt.matcher(caractDiv.html());
            //Parsat pret ca string
            while(infoMatch.find()){
                //Group(1)->Moneda  Group(2)->Price
                String moneda=infoMatch.group(1);
                price = Integer.parseInt(infoMatch.group(2));
                anunt.setPrice(price);
            }
        infoPatt = Pattern.compile("<strong>(.+?):</.+an>(.+?)</span>");
        infoMatch = infoPatt.matcher(caractDiv.html());

            //Parsare informatii
            while(infoMatch.find()){
                //Group(1)->InfoType        Group(2)->Value
                anunt.addCaract(infoMatch.group(1),infoMatch.group(2));
            }

        infoPatt = Pattern.compile("page-subtitle\">Informatii Suplimentare</h2>[\\s]+<p>(.+?)</p>");
        infoMatch = infoPatt.matcher(infoSupl.html());
            if(infoMatch.find()){
                //Group(1)->Extra Info

                String infoSuplimentare = infoMatch.group(1);
                infoSuplimentare = infoSuplimentare.replaceAll("<br>","\n");
                infoSuplimentare = infoSuplimentare.replaceAll("&nbsp;"," ");
                infoSuplimentare = infoSuplimentare.trim();
                anunt.setDescription(infoSuplimentare);
            }
        infoPatt = Pattern.compile("product_title\"> (.+?), <.+> (.+?) </span>");
        infoMatch = infoPatt.matcher(title.html());
            if(infoMatch.find()){
                String anuntTitle = infoMatch.group(1) + "," + infoMatch.group(2);
                anunt.setTitle(anuntTitle);
            }
    }
    public void parseInterestPoints(){
        Elements extraList = pageToParse.select("div[class=zones]");

        Pattern infoPatt = Pattern.compile("titlez\">[\\s]*(.+)[^\\d]+(.+) km[^\\d]+(.+)");
        Matcher infoMatch = infoPatt.matcher(extraList.html());
        //Parsare locatii,distante si timp
        int count = 0;
        while(infoMatch.find()){
            //Group(1)->Location    Group(2)->Distance  Group(3)->Time
            boolean coordFound = false;

            String tempName = infoMatch.group(1);
            double tempDist = Double.parseDouble(infoMatch.group(2));
            String tempTime = infoMatch.group(3);
            InterestPoint tempIP = new InterestPoint(tempName,tempDist);
            for(InterestPoint p : anunt.getInterestPointList()){
                if(p.getName().equals(tempName)){
                    p.setDistance(tempDist);
                    p.setTime(tempTime);
                    coordFound = true;
                }
            }

            if(!coordFound){
                anunt.addInteresPoint(tempIP);
            }
        }
    }

    public Anunt getAnunt(){
        return anunt;
    }
}
