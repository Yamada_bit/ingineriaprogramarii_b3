package Crawler;

import Parser.HTMLParser;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.util.ArrayList;
import java.util.List;

import Anunt.Anunt;
/**
 * Created by donic on 4/8/2016.
 */
public class WebCrawler {
    private List<String> linkList = new ArrayList<String>();
    private List<Anunt> anuntList = new ArrayList<Anunt>();
    public WebCrawler(List<String> list){
        linkList = list;
    }
    public void printList(){
        for(String link : linkList){
            System.out.println(link);
        }
    }
    public void execute() {
        try {
            for(String link : linkList) {
                Document doc = Jsoup.connect(link).userAgent("Mozilla/5.0 (Windows NT 6.1; WOW64; rv:23.0) Gecko/20100101 Firefox/23.0").get();
                HTMLParser parser = new HTMLParser(doc);


                parser.parsePage();
                parser.getAnunt().setUrl(link);
                anuntList.add(parser.getAnunt());
            }

        } catch (Exception ex) {
            System.err.println(ex.getMessage());
        }
    }

    public List<Anunt> getAnuntList(){
        return anuntList;
    }
}
