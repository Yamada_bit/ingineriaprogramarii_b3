/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package database;

import Anunt.Anunt;
import Anunt.InterestPoint;
import exceptions.AppException;
import exceptions.AppSqlDatabaseException;
import java.sql.*;
import java.util.List;

/**
 *
 * @author ciprian
 */
public class Query {
    private Connection conn = null;
    
    public Query(Connection c) {
        this.conn = c;
    }
    
    public void executeInsertQuery(List<Anunt> anunturi) throws AppException {
        
        String insertAnuntQuery = "INSERT INTO anunturi " +
                                "(id_anunt, titlu, tip, pret, zona, latitudine, longitudine, "+
                                "an_constructie, suprafata_teren, suprafata_imobil, numar_camere, descriere) " +
                                "values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
            
        String insertPunctInteresQuery = "INSERT INTO puncte_interes" +
                                        "(id_punct_interes, id_anunt, nume, latitudine, longitudine, distanta, timp) " +
                                        "values (?, ?, ?, ?, ?, ?, ?)";
        
        try {
            PreparedStatement stmt = null;
            int id_anunt = this.countQuery("anunturi") + 1;
            for(Anunt anunt : anunturi) {
                stmt = conn.prepareStatement(insertAnuntQuery);
                
                //Binding
                stmt.setInt     (1, id_anunt);
                stmt.setString  (2, anunt.getTitle());
                stmt.setString  (3, anunt.caractList.get("Tip"));
                stmt.setInt     (4, anunt.getPrice());
                stmt.setString  (5, anunt.caractList.get("Zona"));
                
                if(anunt.getLocation().getLatitude() == 0.0)
                    stmt.setNull(6, java.sql.Types.DOUBLE);
                else
                    stmt.setDouble(6, anunt.getLocation().getLatitude());
                
                if(anunt.getLocation().getLongitude() == 0.0)
                    stmt.setNull(7, java.sql.Types.DOUBLE);
                else
                    stmt.setDouble(7, anunt.getLocation().getLongitude());
                
                stmt.setString  (8, anunt.caractList.get("An constructie"));
                stmt.setString  (9, anunt.caractList.get("Suprafata teren"));
                stmt.setString  (10, anunt.caractList.get("Suprafata"));
                stmt.setInt     (11, Integer.parseInt(anunt.caractList.get("Nr. camere")));
                stmt.setString  (12, anunt.getDescription());
                
                stmt.executeUpdate();
                stmt.close();
                
                //Inserare puncte interes pentru anunt
                int id_punct_interes = this.countQuery("puncte_interes") + 1;
                for(InterestPoint interestPoint : anunt.interestPointList) {
                    PreparedStatement insert_ip_stmt = conn.prepareStatement(insertPunctInteresQuery);
                    //Binding
                    insert_ip_stmt.setInt   (1, id_punct_interes);
                    insert_ip_stmt.setInt   (2, id_anunt);
                    insert_ip_stmt.setString(3, interestPoint.getName());
                    
                    if(interestPoint.getLocation().getLatitude() == 0.0)
                        insert_ip_stmt.setNull(4, java.sql.Types.DOUBLE);
                    else
                        insert_ip_stmt.setDouble(4, interestPoint.getLocation().getLatitude());
                    
                    if(interestPoint.getLocation().getLongitude() == 0.0)
                        insert_ip_stmt.setNull(5, java.sql.Types.DOUBLE);
                    else
                        insert_ip_stmt.setDouble(5, interestPoint.getLocation().getLongitude());
                    
                    insert_ip_stmt.setNull  (6, java.sql.Types.VARCHAR);
                    insert_ip_stmt.setNull  (7, java.sql.Types.VARCHAR);
                    //insert_ip_stmt.setString(6, Double.toString(interestPoint.getDistance()));
                    //insert_ip_stmt.setString(7, interestPoint.getTime());
                    
                    insert_ip_stmt.executeUpdate();
                    insert_ip_stmt.close();
                    id_punct_interes++;
                }
                id_anunt++;
            }
            
        } catch (SQLException ex) {
            throw new AppSqlDatabaseException("from insert query - " + ex.getMessage());
        }
        System.out.printf("Inserted %s anunturi!%n", this.countQuery("anunturi"));
        System.out.printf("Insert query completed!%n");
    }
    
    public void executeDeleteQuery(String table) throws AppSqlDatabaseException {
        try {
            String deleteQuery = "delete from " + table;
            Statement stmt = conn.createStatement();
            stmt.executeUpdate(deleteQuery);
            stmt.close();
            System.out.println("Delete query completed!");
        } catch (SQLException ex) {
           throw new AppSqlDatabaseException("from delete query - " + ex.getMessage());
        }
    }
    
    public int countQuery(String tabel) throws AppSqlDatabaseException
    { 
        try {
            String countQuery = "SELECT COUNT(*) from " + tabel;
            Statement stmt = conn.createStatement();
            ResultSet resultSet = stmt.executeQuery(countQuery);
            
            int count = 0;
            while (resultSet.next()) {
                count = resultSet.getInt(1);
                break;
            }
            resultSet.close();
            stmt.close();
            
            return count;
        } catch (SQLException ex) {
            throw new AppSqlDatabaseException("from count query - " + ex.getMessage());
        }
    }
    
    
    private void printAnuntData(Anunt anunt) {
        
        System.out.printf("id:%d %n", anunt.getId());
        System.out.printf("title:%s %n", anunt.getTitle());
        System.out.printf("Tip imobil: %s%n", anunt.caractList.get("Tip") );
        System.out.printf("pret:%d %n", anunt.getPrice());
        System.out.printf("Zona: %s%n", anunt.caractList.get("Zona") );
        System.out.printf("coordonate  x:%f y:%f %n", 
                anunt.getLocation().getLatitude(), anunt.getLocation().getLongitude());

        System.out.printf("An constructie: %s%n", anunt.caractList.get("An constructie") );
        System.out.printf("Suprafata teren: %s%n", anunt.caractList.get("Suprafata teren") );
        System.out.printf("Suprafata imobil: %s%n", anunt.caractList.get("Suprafata") );
        System.out.printf("Nr camere: %s%n", anunt.caractList.get("Nr. camere") );

        System.out.printf("descriere:%s %n", anunt.getDescription());
        System.out.printf("_______________________________%n");
    }
         
}
