/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package database;

import exceptions.AppException;
import exceptions.AppSqlDatabaseException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 *
 * @author ciprian
 */
public class SqlServerConnection implements SqlConnection {
    private static final String URL = "jdbc:sqlserver://imobiliareB3.mssql.somee.com;" +
         "databaseName=imobiliareB3;user=mihaibaboi_SQLLogin_1;password=x78z191ryt";
    
    
    private Connection connection = null;
    public SqlServerConnection() throws AppException {
        try {
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
        } catch (ClassNotFoundException ex) {
            throw new AppException("class driver not found!");
        }
    }
    
    @Override
    public void initConnection() throws AppSqlDatabaseException {
        
        try {
            connection = DriverManager.getConnection(URL);
        } catch (SQLException ex) {
            throw new AppSqlDatabaseException(ex.getMessage());
        }
        
    }  
    
    @Override
    public void closeResources() throws AppSqlDatabaseException {
        try {
            getConnection().close();
        } catch (SQLException ex) {
            throw new AppSqlDatabaseException(ex.getMessage());
        }
    }

    /**
     * @return the connection
     */
    @Override
    public Connection getConnection() {
        return connection;
    }
}
