/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package database;

import exceptions.*;
import java.sql.*;


/**
 *
 * @author ciprian
 */
public class MySqlConnection implements SqlConnection {
    
    private static final String URL = "jdbc:mysql://192.168.1.234:3306/crawlingdb";
    private static final String USER = "crawler";
    private static final String PASS = "toor"; 
    
    
    private Connection connection = null;
    public MySqlConnection() throws AppException {
        try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (ClassNotFoundException ex) {
            throw new AppException("Class driver not found");
        }
    }
    
    @Override
    public void initConnection() throws AppSqlDatabaseException {
        
        try {
            connection = DriverManager.getConnection(URL, USER, PASS);
        } catch (SQLException ex) {
            throw new AppSqlDatabaseException(ex.getMessage());
        }
        
    }  
    
    @Override
    public void closeResources() throws AppSqlDatabaseException {
        try {
            getConnection().close();
        } catch (SQLException ex) {
            throw new AppSqlDatabaseException(ex.getMessage());
        }
    }

    /**
     * @return the connection
     */
    @Override
    public Connection getConnection() {
        return connection;
    }
}
