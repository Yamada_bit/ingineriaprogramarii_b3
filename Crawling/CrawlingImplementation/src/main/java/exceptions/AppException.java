/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exceptions;

/**
 *
 * @author ciprian
 */
public class AppException extends Exception {
    
    private AppException() {
        super("Error: An unknown error has appeared!");
    }
    
    public AppException(String msg) {
        super("Error " + msg + "!");
    }
}
