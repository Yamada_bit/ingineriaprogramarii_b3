import Anunt.*;
import Crawler.WebCrawler;
import database.*;
import exceptions.AppException;

import java.io.*;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by donic on 4/8/2016.
 */
public class Main {

    public static void main(String args[]) {
        List<Anunt> list = new ArrayList<Anunt>();
        List<String> sources = new ArrayList<String>();
        sources = getLinks(1);

        //Crawler
        WebCrawler spider = new WebCrawler(sources);
        spider.execute();
        list = spider.getAnuntList();
        
        //Database
        try {
            SqlConnection db = new MySqlConnection();
            db.initConnection();
            Query query = new Query(db.getConnection());
            //query.countQuery("anunturi");
            query.executeInsertQuery(list);
            //query.executeDeleteQuery("anunturi");
            db.closeResources();
        } catch (AppException ex) {
            System.err.println(ex.getMessage());
        }
    }

    /**
     * Read from file the links and
     * @return  The list of links to crawl
     */
    public static List<String> getLinks(int limit){
        List<String> links = new ArrayList<String>();
        BufferedReader buffer = null;
        String url = null;
        URL fileUrl = Main.class.getResource("links.txt");
        File file = new File(fileUrl.getPath());
        
        try {
            buffer = new BufferedReader(new FileReader(file));
            int count = 0; //pentru test in db
            while ((url = buffer.readLine()) != null && count < limit) {
                //url = "\"" + url + "\"";
                links.add(url);
                count++;
            }
        }
        catch (IOException e) {
            e.printStackTrace();
        }
        finally {
            try {
                if (buffer != null) buffer.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return links;
    }

    public static void exportAnunt(List<Anunt> lista){
        int count = 1;
        for(Anunt a : lista){
            String mainLocX,mainLocY;
            if(a.getLocation().getLatitude() == 0) {
                mainLocX = "NULL";
                mainLocY = "NULL";
            }
            else{
                //Format location to have exact 5 decimals
                mainLocX = String.format("%7.5f", a.getLocation().getLongitude());
                mainLocY = String.format("%7.5f", a.getLocation().getLatitude());
            }
            String ipCount = "18";
            try {
                File yourFile = new File("data.txt");
                if(!yourFile.exists()) {
                    yourFile.createNewFile();
                }
                FileWriter fw = new FileWriter(yourFile.getAbsoluteFile(),true);
                BufferedWriter bw = new BufferedWriter(fw);

                bw.append("["+count+"] ");
                bw.append(mainLocX + " " + mainLocY + "\n");
                bw.append(ipCount + "\n");

                    for(InterestPoint p : a.getInterestPointList()){
                        String interestPointLocX,interestPointLocY;
                        String interestPointDist = Double.toString(p.getDistance());
                        String interestPointName = p.getName().replace(" ","_");
                        if(mainLocX.equals("NULL")) {
                            interestPointLocX ="NULL";
                            interestPointLocY = "NULL";
                        }
                        else{
                            interestPointLocX = String.format("%7.5f", p.getLocation().getLongitude());
                            interestPointLocY = String.format("%7.5f", p.getLocation().getLatitude());
                        }
                        bw.append(interestPointName + " " + interestPointLocX + " " + interestPointLocY + " " + interestPointDist + "\n");
                    }
                bw.close();
                count++;
            }
            catch(Exception e){
                e.printStackTrace();
            }
        }
    }
}
