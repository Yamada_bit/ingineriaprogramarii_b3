package Anunt;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by donic on 4/9/2016.
 */
public class Anunt {
        private static int count = 0;
        private Location location = new Location();
        private String url;
        private String title;
        private int id;
        private int price;
        private String description;
        public List<InterestPoint> interestPointList= new ArrayList<InterestPoint>();
        public Map<String,String> caractList = new HashMap<String,String>();

        public Anunt(){
            count++;
            id = count;
            title = "Anunt " + count;
        }
        public void addInteresPoint(InterestPoint ip){
            interestPointList.add(ip);
        }

        public void setDescription(String var){description = var;}
        public void setUrl(String var){ url = var;}
        public void setTitle(String var) {title = var;}
        public void setId (int var) {id = var;}
        public void setPrice (int var) {price = var;}
        public void setLocation(Location l){
            location.setLongitude(l.getLongitude());
            location.setLatitude(l.getLatitude());
        }
        public void setLocation (double x,double y) {
            if(x > 0 && y > 0) {
                location.setLatitude(x);
                location.setLongitude(y);
            }
        }

        public String getDescription(){return description;};
        public Location getLocation() {return location;}
        public String getUrl(){return url;}
        public String getTitle(){return title;}
        public int getPrice(){return price;}
        public int getId(){return id;}
        public List<InterestPoint> getInterestPointList(){
            return interestPointList;
        }

        public void addCaract(String infoType,String value){
            this.caractList.put(infoType,value);
        }
}
