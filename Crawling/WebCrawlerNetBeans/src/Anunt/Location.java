package Anunt;

/**
 * Created by donic on 4/9/2016.
 */
public class Location {
    private double latitude,longitude;
    public Location(){
        latitude = 0;
        longitude = 0;
    }

    public Location(double y, double x){
        latitude = y;
        longitude = x;
    }

    public void setLatitude(double x){
        latitude = x;
    }
    public void setLongitude(double y){
        longitude = y;
    }
    public double getLatitude(){
        return latitude;
    }
    public double getLongitude(){
        return longitude;
    }
}
