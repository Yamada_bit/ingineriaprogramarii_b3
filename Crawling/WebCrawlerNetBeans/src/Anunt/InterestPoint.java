package Anunt;

/**
 * Created by donic on 4/9/2016.
 */
public class InterestPoint {
    
    private String type = "Default";
    private String name;
    private Location location;
    private double  distance;
    private String time;
    public InterestPoint(String place,double dist){
        name = place;
        distance = dist;
        location = new Location();
    }
    public InterestPoint(String place,Location loc){
        name = place;
        location = loc;
        distance = 0.0;
    }
    public InterestPoint(String place,Location loc,double dist){
        name = place;
        location = loc;
        distance = dist;
    }

    public void setDistance(double dist){
        distance = dist;
    }
    public void setTime(String tim){
        time = tim;
    }
    
    public void setType(String type){
        this.type = type;
    }

    public String getName(){return name;}
    public double getDistance(){return distance;}
    public Location getLocation(){return location;}
    public String getTime(){return time;}
    public String getType(){return type;}

}
