/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package database;

import Anunt.Anunt;
import Anunt.InterestPoint;
import exceptions.AppException;
import exceptions.AppSqlDatabaseException;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author ciprian
 */
public class Query {
    private Connection conn = null;
    
    public Query(Connection c) {
        this.conn = c;
    }
    
    private int getIdFacilitate(Double x, Double y, String nume) throws AppSqlDatabaseException {

        int id_facilitate = 0;      //nu exista facilitatea
        try {
            PreparedStatement stmt = null;
            String selectIdFacilitate = null;
            if(x == 0.0 || y == 0.0) {
                selectIdFacilitate = "SELECT id_facilitate from Facilitati where nume = ?";
                stmt = conn.prepareStatement(selectIdFacilitate);
                stmt.setString(1, nume);
            }   
            else {
                selectIdFacilitate = "SELECT id_facilitate from Facilitati " +
                                            "where nume = ? and latitudine = ? and longitudine = ?";
                stmt = conn.prepareStatement(selectIdFacilitate);
                stmt.setString(1, nume);
                stmt.setDouble(2, x);
                stmt.setDouble(3, y);
                
            }
            
            ResultSet resultSet = stmt.executeQuery();
            while (resultSet.next()) {
                id_facilitate = resultSet.getInt(1);
                break;
            }
           resultSet.close();
           stmt.close();
        } catch (SQLException ex) {
            throw new AppSqlDatabaseException("from 'select id_facilitate' query - " + ex.getMessage());
        }
        return id_facilitate;
    }
    
    public void insertData(List<Anunt> anunturi) throws AppException {
        
        String insertAnuntQuery = "INSERT INTO Anunturi " +
                                "(id_anunt, url, titlu, tip, pret, zona, latitudine, longitudine, "+
                                "an_constructie, suprafata_teren, suprafata_imobil, numar_camere, descriere) " +
                                "values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
            
        try {
            PreparedStatement stmt = null;
            int id_anunt = this.countQuery("Anunturi") + 1;
            for(Anunt anunt : anunturi) {
                stmt = conn.prepareStatement(insertAnuntQuery);
                //Binding
                stmt.setInt     (1, id_anunt);
                stmt.setString  (2, anunt.getUrl());
                stmt.setString  (3, anunt.getTitle());
                stmt.setString  (4, anunt.caractList.get("Tip"));
                stmt.setInt     (5, anunt.getPrice());
                stmt.setString  (6, anunt.caractList.get("Zona"));
                if(anunt.getLocation().getLatitude() == 0.0)
                    stmt.setNull(7, java.sql.Types.DOUBLE);
                else
                    stmt.setDouble(7, anunt.getLocation().getLatitude());
                if(anunt.getLocation().getLongitude() == 0.0)
                    stmt.setNull(8, java.sql.Types.DOUBLE);
                else
                    stmt.setDouble(8, anunt.getLocation().getLongitude());
                stmt.setString  (9, anunt.caractList.get("An constructie"));
                stmt.setString  (10, anunt.caractList.get("Suprafata teren"));
                stmt.setString  (11, anunt.caractList.get("Suprafata"));
                stmt.setString  (12, anunt.caractList.get("Nr. camere"));
                stmt.setString  (13, anunt.getDescription());
                stmt.executeUpdate();
                stmt.close();
                
                //Inserare puncte interes pentru anunt
                for(InterestPoint interestPoint : anunt.interestPointList) {
                    //Se cauta daca facilitatea nu exista deja
                    int idFacilitate = this.getIdFacilitate(anunt.getLocation().getLatitude()
                            , anunt.getLocation().getLongitude(), interestPoint.getName());
                    
                    //Daca facilitatea nu exista se insereaza una noua
                    if(idFacilitate == 0) {
                        String insertFacilitateQuery = "INSERT INTO Facilitati " +
                                        "(id_facilitate, tip, nume, latitudine, longitudine) " +
                                        "values (?, ?, ?, ?, ?)";
                        idFacilitate = this.countQuery("Facilitati") + 1;
                        PreparedStatement insert_facilitate_stmt = conn.prepareStatement(insertFacilitateQuery);
                        insert_facilitate_stmt.setInt(1, idFacilitate);
                        insert_facilitate_stmt.setString(2, interestPoint.getType());
                        insert_facilitate_stmt.setString(3, interestPoint.getName());
                        if(interestPoint.getLocation().getLatitude() == 0.0)
                            insert_facilitate_stmt.setNull(4, java.sql.Types.DOUBLE);
                        else
                            insert_facilitate_stmt.setDouble(4, interestPoint.getLocation().getLatitude());
                        if(interestPoint.getLocation().getLongitude() == 0.0)
                            insert_facilitate_stmt.setNull(5, java.sql.Types.DOUBLE);
                        else
                            insert_facilitate_stmt.setDouble(5, interestPoint.getLocation().getLongitude());
                        insert_facilitate_stmt.executeUpdate();
                        insert_facilitate_stmt.close();
                    } //End inserare facilitate noua in caz ca nu exista
                    
                    String insert_Anunt_Facilitate_Link_query = "insert into AnunturiFacilitati " +
                            "(id_anunt, id_facilitate, distanta, timp) values (?, ?, ?, ?)";
                    PreparedStatement insert_Anunt_Facilitate_Link_stmt = null;
                    insert_Anunt_Facilitate_Link_stmt = conn.prepareStatement(insert_Anunt_Facilitate_Link_query);
                    insert_Anunt_Facilitate_Link_stmt.setInt(1, id_anunt);
                    insert_Anunt_Facilitate_Link_stmt.setInt(2, idFacilitate);
                    insert_Anunt_Facilitate_Link_stmt.setDouble(3, interestPoint.getDistance());
                    insert_Anunt_Facilitate_Link_stmt.setString(4, interestPoint.getTime());
                    insert_Anunt_Facilitate_Link_stmt.executeUpdate();
                    insert_Anunt_Facilitate_Link_stmt.close();
                }
                System.out.format("Completed anunt with id = %d.%n", id_anunt);
                id_anunt++;
            }
            
        } catch (SQLException ex) {
            throw new AppSqlDatabaseException("from insert query - " + ex.getMessage());
        }
        System.out.printf("Inserted %s anunturi!%n", this.countQuery("Anunturi"));
        System.out.printf("Insert query completed!%n");
    }
    
    public void executeDeleteQuery(String table) throws AppSqlDatabaseException {
        try {
            String deleteQuery = "delete from " + table;
            Statement stmt = conn.createStatement();
            stmt.executeUpdate(deleteQuery);
            stmt.close();
            System.out.println("Delete query completed!");
        } catch (SQLException ex) {
           throw new AppSqlDatabaseException("from delete query - " + ex.getMessage());
        }
    }
    
    public int countQuery(String tabel) throws AppSqlDatabaseException
    { 
        try {
            String countQuery = "SELECT COUNT(*) from " + tabel;
            Statement stmt = conn.createStatement();
            ResultSet resultSet = stmt.executeQuery(countQuery);
            
            int count = 0;
            while (resultSet.next()) {
                count = resultSet.getInt(1);
                break;
            }
            resultSet.close();
            stmt.close();
            
            return count;
        } catch (SQLException ex) {
            throw new AppSqlDatabaseException("from count query - " + ex.getMessage());
        }
    }
    
    
    public void executeSelectStarQuery(String table) throws AppSqlDatabaseException {
    
        try {
            String query = "SELECT * from " + table;
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(query);
            ResultSetMetaData metadata = rs.getMetaData();
            
            List<String> columns = new ArrayList<>();
            for (int i = 1; i <= metadata.getColumnCount(); i++)
                columns.add(metadata.getColumnName(i));  
            
            while (rs.next()) {
                String row = "";
                for(String column : columns) {
                    System.out.printf("%s    ", rs.getString(column));
                }
                System.out.println();
            }
        }
        catch( SQLException ex) {
            throw new AppSqlDatabaseException("from 'select * query' - " + ex.getMessage());
        }
    
    }
         
}
