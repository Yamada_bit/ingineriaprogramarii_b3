/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package database;

import exceptions.AppSqlDatabaseException;
import java.sql.Connection;

/**
 *
 * @author ciprian
 */
public interface SqlConnection {
    
    public void initConnection() throws AppSqlDatabaseException;
    public void closeResources() throws AppSqlDatabaseException;
    public Connection getConnection();
}
