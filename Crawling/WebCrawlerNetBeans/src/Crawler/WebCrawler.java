package Crawler;

import Parser.HTMLParser;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.util.ArrayList;
import java.util.List;
import app.Link;

import Anunt.Anunt;
/**
 * Created by donic on 4/8/2016.
 */
public class WebCrawler {
    private List<Link> linkList = new ArrayList<>();
    private List<Anunt> anuntList = new ArrayList<Anunt>();
    public WebCrawler(List<Link> list){
        linkList = list;
    }
    public void printList(){
        for(Link link : linkList){
            System.out.println(link.getLink());
        }
    }
    public void execute() {
        try {
            for(Link link : linkList) {
                Document doc = Jsoup.connect(link.getLink()).userAgent("Mozilla/5.0 (Windows NT 6.1; WOW64; rv:23.0) Gecko/20100101 Firefox/23.0").get();
                HTMLParser parser = new HTMLParser(doc);


                parser.parsePage();
                parser.getAnunt().setUrl(link.getLink());
                anuntList.add(parser.getAnunt());
            }

        } catch (Exception ex) {
            System.err.println(ex.getMessage());
        }
    }

    public List<Anunt> getAnuntList(){
        return anuntList;
    }
}
