package app;

import Anunt.*;
import Crawler.WebCrawler;
import database.*;
import exceptions.AppException;
import exceptions.AppSqlDatabaseException;

import java.io.*;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * Created by donic on 4/8/2016.
 */
public class Main {

    public static void main(String args[]) throws AppSqlDatabaseException {
        Scanner input = new Scanner(System.in);
        List<Anunt> list = new ArrayList<Anunt>();
        List<Link> sources = new ArrayList<>();
        
        System.out.format("How many pages to load: ");
        int numberOfPages = input.nextInt();
        sources = getLinks(numberOfPages);
        WebCrawler spider = new WebCrawler(sources);
        spider.execute();
        list = spider.getAnuntList();
        System.out.format("Crawling operation completed, %s pages parsed succesfully!%n", sources.size());
        //printData(list);  
        exportAnunt(list);
        SqlConnection db = null;
        //Database
        try {
            db = new MySqlConnection();
            db.initConnection();
            Query query = new Query(db.getConnection());
            
            //query.executeSelectStarQuery("Facilitati");
            query.insertData(list);
            //query.executeDeleteQuery("AnunturiFacilitati");
            //query.executeDeleteQuery("Facilitati");
            //query.executeDeleteQuery("Anunturi");
            System.out.println("Rows count: " + query.countQuery("Anunturi") );
            
        } catch (AppException ex) {
            System.err.println(ex.getMessage());
        } finally {
            db.closeResources();
        }
    }

    public static void printData(List<Anunt> list) {
        
        for(Anunt a: list) {
            System.out.printf("URL: %s%n", a.getUrl());
            System.out.printf("Titlu: %s%n", a.getTitle());
            System.out.printf("Coordonate: x:%f  y:%f%n", a.getLocation().getLatitude(), a.getLocation().getLongitude());
            System.out.printf("Nr pct interes: %d%n", a.interestPointList.size());
            for(InterestPoint p: a.interestPointList) {
                System.out.printf("Tip: %s%n", p.getType());
                System.out.printf("Nume: %s%n", p.getName());
                System.out.printf("Coordonate pi: x:%f  y:%f%n", p.getLocation().getLatitude(), p.getLocation().getLongitude());
            }
            System.out.println("-------------------------------");
        }
    }
    
    
    
    /**
     * Read from file the links and
     * @return  The list of links to crawl
     */
    public static List<Link> getLinks(int limit){
        
        List<Link> links = new ArrayList<>();
        
        
        BufferedReader buffer = null;
        String url = null;
        URL fileUrl = Main.class.getResource("links.txt");
        File file = new File(fileUrl.getPath());
        
        try {
            buffer = new BufferedReader(new FileReader(file));
            int count = 0; //pentru test in db
            while ((url = buffer.readLine()) != null && count < limit) {
                links.add(new Link(url));
                count++;
            }
        }
        catch (IOException e) {
            e.printStackTrace();
        }
        finally {
            try {
                if (buffer != null) buffer.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return links;
    }

    public static void exportAnunt(List<Anunt> lista){
        int count = 1;
        for(Anunt a : lista){
            
            try {
                File yourFile = new File("data.txt");
                if(!yourFile.exists()) {
                    yourFile.createNewFile();
                }
                FileWriter fw = new FileWriter(yourFile.getAbsoluteFile(),true);
                BufferedWriter bw = new BufferedWriter(fw);
                
                bw.append(a.getDescription() + "\n----------------------------------------------\n");

                    
                bw.close();
                count++;
            }
            catch(Exception e){
                e.printStackTrace();
            }
        }
    }
}
