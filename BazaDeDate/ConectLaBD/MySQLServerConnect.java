import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Properties;

/*!trebuie inclus sqljdbc4-2.0.jar*/
public class MySQLServerConnect {
    // init database constants
    private static final String DATABASE_DRIVER = "com.microsoft.sqlserver.jdbc.SQLServerDriver";
    private static final String USERNAME = "mihaibaboi_SQLLogin_1";
    private static final String PASSWORD = "x78z191ryt";
    private static final String MAX_POOL = "250";
    private static final String url = "jdbc:sqlserver://imobiliareB3.mssql.somee.com:1433;"
    		+ "databaseName=imobiliareB3;user=mihaibaboi_SQLLogin_1;password=x78z191ryt";

    // init connection object
    private Connection connection;
    // init properties object
    private Properties properties;

    // create properties
    @SuppressWarnings("unused")
	private Properties getProperties() {
        if (properties == null) {
            properties = new Properties();
            properties.setProperty("user", USERNAME);
            properties.setProperty("password", PASSWORD);
            properties.setProperty("MaxPooledStatements", MAX_POOL);
        }
        return properties;
    }

    // connect database
    public Connection connect() {
        if (connection == null) {
            try {
                Class.forName(DATABASE_DRIVER);
                connection = DriverManager.getConnection(url);
                System.out.println("Connected...");
            } catch (ClassNotFoundException | SQLException e) {
                e.printStackTrace();
            }
        }
        return connection;
    }

    // disconnect database
    public void disconnect() {
        if (connection != null) {
            try {
                connection.close();
                connection = null;
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }
    
    //readin from a table
    public void test() {
    	String READ_OBJECT_SQL = "SELECT nume FROM Test";
    	PreparedStatement pstmt;
		try {
			pstmt = connection.prepareStatement(READ_OBJECT_SQL);
			ResultSet rs = pstmt.executeQuery();
		    rs.next();
		    String is = rs.getString(1) ;
		    System.out.println(is);
		    rs.close();
		    pstmt.close();
		    connection.commit();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
}