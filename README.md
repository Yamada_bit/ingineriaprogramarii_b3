GoogleSpreadsheet:
https://docs.google.com/spreadsheets/d/10tWyif0qNv4ee1M-u8QnHrkPuzfTjFLCu8XyzWH33xw/edit?usp=sharing


# README #

### Universitatea Alexandru Ioan Cuza Iasi  ###
### Facultatea de Informatica Iasi ###

Proiect: Imobiliare recomandari

Cerinte:
1) Crawling la date si sa va populati o baza de date	 
2) Pe langa informatiile de pe site ma intereseaza sa veniti cu informatii aditionale, care pot fi deduse din acele informatii		
a) Localizarea cat mai precisa folosind triunghiularizare		
b) Calitatea aerului		
c) distanta catre cea mai apropiata padure, teren de sport		
d) procesari textuale asupra descrierii, care sa-mi dea factori de interes (renovari, spatiu verde) sau dezinteres asupra proprietatii (diferenta mica dintre spatiul proprietatii si spatiul casei)		
3) Obtinere profil cautator + oferire recomandari

Exemplu: http://www.fideliacasa.ro/

Dificultate: ***

Module: Crawling
	LocalizareGPS
	BazeDeDate
	CalitateaAerului
	Profiling&Recomandations